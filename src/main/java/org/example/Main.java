package org.example;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.formula.functions.Index;
import org.apache.poi.ss.usermodel.*;


import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellReference;

import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFColor;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;

public class Main {
    public static void main(String[] args) throws IOException {
        // window
        // final String excelFilePath = "C:/demo/books.xlsx";

        // macos
        final String excelFilePath = "/Users/khanhquang/Documents/HCMUT/Intern/out/monthly.xlsx";
        // System.out.println(excelFilePath); // test path excel

        // write file excel
        writeExcel(excelFilePath);
        System.out.println("Done!!!");
    }

    public static void writeExcel(String excelFilePath) throws IOException {
        // Create Workbook
        SXSSFWorkbook workbook = new SXSSFWorkbook();

        // Create sheets
        generateSheets(workbook);

        // Create file excel
        createOutputFile(workbook, excelFilePath);
    }

    // Create output file
    private static void createOutputFile(SXSSFWorkbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }

    private static void generateSheets(SXSSFWorkbook workbook){
        // SXSSFSheet sheet1 = workbook.createSheet("Guideline");
        SXSSFSheet sheet2 = workbook.createSheet("Courses, seminars, workshops");
        // SXSSFSheet sheet3 = workbook.createSheet("Exam & Certification support");
        // SXSSFSheet sheet4 = workbook.createSheet("Record of changes");
        // SXSSFSheet sheet5 = workbook.createSheet("Pivots");

        // write sheets
        // writeGuideline(sheet1);
        writeSeminars(sheet2);
        // writeExams(sheet3);
        // writeChanges(sheet4);
        // writePivots(sheet5);
    }
    private static void writeGuideline(SXSSFSheet sheet){
        System.out.println("waiting for late");
    }

    private static void writeSeminars(SXSSFSheet sheet){
        System.out.println("write seminars");
    }

    private static void writeExams(SXSSFSheet sheet){
        System.out.println("write exams");
    }

    private static void writeChanges(SXSSFSheet sheet){
        sheet.setDisplayGridlines(false);
        // variables
        final int ROW_IDX_TITLE = 0;
        final int ROW_IDX_HEADER = 2;
        final int COLUMN_IDX_TITLE = 1;
        final int COLUMN_IDX_NO = 0;
        final int COLUMN_IDX_DATE = 1;
        final int COLUMN_IDX_VERSION = 2;
        final int COLUMN_IDX_CHANGE = 3;
        final int COLUMN_IDX_REASON = 4;
        final int COLUMN_IDX_REVIEWER = 5;
        final int COLUMN_IDX_APPROVER = 6;
        final int COLUMN_WIDTH_NO = 1750;
        final int COLUMN_WIDTH_DATE = 3500;
        final int COLUMN_WIDTH_VERSION = 2300;
        final int COLUMN_WIDTH_CHANGE = 17500;
        final int COLUMN_WIDTH_REASON = 8500;
        final int COLUMN_WIDTH_REVIEWER = 5000;
        final int COLUMN_WIDTH_APPROVER = 5000;
        final CellStyle cellStyle = createStyleforHeaderChanges(sheet);
        CellStyle bold = sheet.getWorkbook().createCellStyle();

        // write header
        Row titleRow = sheet.createRow(ROW_IDX_TITLE);
        Row headerRow = sheet.createRow(ROW_IDX_HEADER);

        Cell titleCell = titleRow.createCell(COLUMN_IDX_TITLE);
        Cell headerCell0 = headerRow.createCell(COLUMN_IDX_NO);
        Cell headerCell1 = headerRow.createCell(COLUMN_IDX_DATE);
        Cell headerCell2 = headerRow.createCell(COLUMN_IDX_VERSION);
        Cell headerCell3 = headerRow.createCell(COLUMN_IDX_CHANGE);
        Cell headerCell4 = headerRow.createCell(COLUMN_IDX_REASON);
        Cell headerCell5 = headerRow.createCell(COLUMN_IDX_REVIEWER);
        Cell headerCell6 = headerRow.createCell(COLUMN_IDX_APPROVER);

        titleCell.setCellValue("RECORD OF CHANGES");
        headerCell0.setCellValue("No");
        headerCell1.setCellValue("Effective Date");
        headerCell2.setCellValue("Version");
        headerCell3.setCellValue("Change description");
        headerCell4.setCellValue("Reason");
        headerCell5.setCellValue("Reviewer");
        headerCell6.setCellValue("Approver");

        sheet.setColumnWidth(COLUMN_IDX_NO, COLUMN_WIDTH_NO);
        sheet.setColumnWidth(COLUMN_IDX_DATE, COLUMN_WIDTH_DATE);
        sheet.setColumnWidth(COLUMN_IDX_VERSION, COLUMN_WIDTH_VERSION);
        sheet.setColumnWidth(COLUMN_IDX_CHANGE, COLUMN_WIDTH_CHANGE);
        sheet.setColumnWidth(COLUMN_IDX_REASON, COLUMN_WIDTH_REASON);
        sheet.setColumnWidth(COLUMN_IDX_REVIEWER, COLUMN_WIDTH_REVIEWER);
        sheet.setColumnWidth(COLUMN_IDX_APPROVER, COLUMN_WIDTH_APPROVER);

        titleCell.setCellStyle(bold);
        headerCell0.setCellStyle(cellStyle);
        headerCell1.setCellStyle(cellStyle);
        headerCell2.setCellStyle(cellStyle);
        headerCell3.setCellStyle(cellStyle);
        headerCell4.setCellStyle(cellStyle);
        headerCell5.setCellStyle(cellStyle);
        headerCell6.setCellStyle(cellStyle);

        // data
        // TODO:
    }

    private static CellStyle createStyleforHeaderChanges(Sheet sheet){
        // create font
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);

        // create cellstyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);

        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // border
        borderInChanges(new CellRangeAddress(2, 2, 0, 6), sheet);

        return cellStyle;

    }

    private static void writePivots(SXSSFSheet sheet){
        sheet.setDisplayGridlines(false);
        /*
         * VARIABLES
         */
        final int columnA = 0;
        final int columnB = 1;
        final int columnC = 2;
        final int columnD = 3;
        final int columnE = 4;
        final int columnF = 5;
        final int columnG = 6;
        final int columnH = 7;
        final int columnI = 8;
        final int columnJ = 9;
        final int columnK = 10;
        // final int columnL = 11;
        // final int columnM = 12;
        final int columnN = 13;
        final int columnO = 14;
        final int columnP = 15;
        final int columnQ = 16;

        Row row1 = sheet.createRow(0);
        Row row2 = sheet.createRow(1);
        Row row3 = sheet.createRow(2);
        Row row4 = sheet.createRow(3);
        Row row5 = sheet.createRow(4);
        Row row6 = sheet.createRow(5);
        Row row7 = sheet.createRow(6);
        Row row8 = sheet.createRow(7);
        Row row9 = sheet.createRow(8);
        Row row11 = sheet.createRow(10);
        row11.setHeight((short) 1500);

        Row row36 = sheet.createRow(35);
        Row row37 = sheet.createRow(36);
        Row row38 = sheet.createRow(37);
        Row row39 = sheet.createRow(38);
        Row row40 = sheet.createRow(39);
        Row row41 = sheet.createRow(40);
        Row row42 = sheet.createRow(41);
        Row row44 = sheet.createRow(43);
        row44.setHeight((short) 1500);

        // cells for left section
        Cell A1 = row1.createCell(columnA);
        Cell A2 = row2.createCell(columnA);

        Cell A3 = row3.createCell(columnA);
        Cell A4 = row4.createCell(columnA);
        Cell A5 = row5.createCell(columnA);
        Cell A6 = row6.createCell(columnA);
        Cell A7 = row7.createCell(columnA);
        Cell A8 = row8.createCell(columnA);
        Cell A9 = row9.createCell(columnA);

        Cell B3 = row3.createCell(columnB);
        Cell B4 = row4.createCell(columnB);
        Cell B5 = row5.createCell(columnB);
        Cell B6 = row6.createCell(columnB);
        Cell B7 = row7.createCell(columnB);
        Cell B8 = row8.createCell(columnB);
        Cell B9 = row9.createCell(columnB);

        Cell A11 = row11.createCell(columnA);
        Cell B11 = row11.createCell(columnB);
        Cell C11 = row11.createCell(columnC);
        Cell D11 = row11.createCell(columnD);
        Cell E11 = row11.createCell(columnE);
        Cell F11 = row11.createCell(columnF);
        Cell G11 = row11.createCell(columnG);
        Cell H11 = row11.createCell(columnH);
        Cell I11 = row11.createCell(columnI);
        Cell J11 = row11.createCell(columnJ);
        Cell K11 = row11.createCell(columnK);

        Cell A36 = row36.createCell(columnA);
        Cell A37 = row37.createCell(columnA);
        Cell A38 = row38.createCell(columnA);
        Cell A39 = row39.createCell(columnA);
        Cell A40 = row40.createCell(columnA);
        Cell A41 = row41.createCell(columnA);
        Cell A42 = row42.createCell(columnA);

        Cell B37 = row37.createCell(columnB);
        Cell B38 = row38.createCell(columnB);
        Cell B39 = row39.createCell(columnB);
        Cell B40 = row40.createCell(columnB);
        Cell B41 = row41.createCell(columnB);
        Cell B42 = row42.createCell(columnB);

        Cell A44 = row44.createCell(columnA);
        Cell B44 = row44.createCell(columnB);
        Cell C44 = row44.createCell(columnC);
        Cell D44 = row44.createCell(columnD);
        Cell E44 = row44.createCell(columnE);
        Cell F44 = row44.createCell(columnF);
        Cell G44 = row44.createCell(columnG);
        Cell H44 = row44.createCell(columnH);
        Cell I44 = row44.createCell(columnI);
        Cell J44 = row44.createCell(columnJ);
        Cell K44 = row44.createCell(columnK);

        // cells for right section
        Cell N1 = row1.createCell(columnN);
        Cell N2 = row2.createCell(columnN);

        Cell N5 = row5.createCell(columnN);
        Cell N6 = row6.createCell(columnN);
        Cell N7 = row7.createCell(columnN);
        Cell N8 = row8.createCell(columnN);
        Cell N9 = row9.createCell(columnN);

        Cell O5 = row5.createCell(columnO);
        Cell O6 = row6.createCell(columnO);
        Cell O7 = row7.createCell(columnO);
        Cell O8 = row8.createCell(columnO);
        Cell O9 = row9.createCell(columnO);

        Cell N11 = row11.createCell(columnN);
        Cell O11 = row11.createCell(columnO);
        Cell P11 = row11.createCell(columnP);
        Cell Q11 = row11.createCell(columnQ);

        Cell N36 = row36.createCell(columnN);
        Cell N38 = row38.createCell(columnN);
        Cell N39 = row39.createCell(columnN);
        Cell N40 = row40.createCell(columnN);
        Cell N41 = row41.createCell(columnN);
        Cell N42 = row42.createCell(columnN);

        Cell O38 = row38.createCell(columnO);
        Cell O39 = row39.createCell(columnO);
        Cell O40 = row40.createCell(columnO);
        Cell O41 = row41.createCell(columnO);
        Cell O42 = row42.createCell(columnO);

        Cell N44 = row44.createCell(columnN);
        Cell O44 = row44.createCell(columnO);
        Cell P44 = row44.createCell(columnP);
        Cell Q44 = row44.createCell(columnQ);
        /*
         * SET WIDTH COLUMN
         */
        int[] arrLengthOfColumn = {6200, 3200, 3200, 3200, 3200, 3200, 3200, 3200,
                                    3200, 3200, 3200, 750, 750, 6200, 3200, 3200, 3200};
        for (int idx = 0 ; idx < 17; idx++){
            sheet.setColumnWidth(idx, arrLengthOfColumn[idx]);
        }

        /*
         * LEFT SECTIONS IMPLEMENT
         */

        // header
        A1.setCellValue("1. Courses, seminars, workshops");
        A2.setCellValue("1.1 By Start year & month");

        // table
        A3.setCellValue("Site");
        A4.setCellValue("Attendee type");
        A5.setCellValue("Format type");
        A6.setCellValue("Scope");
        A7.setCellValue("Delivery type");
        A8.setCellValue("Course status");
        A9.setCellValue("Start Year");

        B3.setCellValue("(All)");
        B4.setCellValue("(All)");
        B5.setCellValue("(All)");
        B6.setCellValue("(All)");
        B7.setCellValue("(All)");
        B8.setCellValue("(All)");
        B9.setCellValue("(All)");

        // content
        A11.setCellValue("Row Labels");
        B11.setCellValue("Count of Course code");
        C11.setCellValue("Sum of Learning time (hrs)");
        D11.setCellValue("Sum of Number of trainees");
        E11.setCellValue("Sum of Number of enrolled trainees");
        F11.setCellValue("Sum of Number of graduates");
        G11.setCellValue("Sum of expense");
        H11.setCellValue("Average of Training feedback");
        I11.setCellValue("Average of Training feedback - Content");
        J11.setCellValue("Average of Training feedback - Teacher");
        K11.setCellValue("Average of Training feedback - Organization");

        // table 2
        A36.setCellValue("1.2 By End year & month");
        A37.setCellValue("Site");
        A38.setCellValue("Attendee type");
        A39.setCellValue("Format type");
        A40.setCellValue("Scope");
        A41.setCellValue("Delivery type");
        A42.setCellValue("End Year");

        B37.setCellValue("(All)");
        B38.setCellValue("(All)");
        B39.setCellValue("(All)");
        B40.setCellValue("(All)");
        B41.setCellValue("(All)");
        B42.setCellValue("(All)");

        A44.setCellValue("Row Labels");
        B44.setCellValue("Count of Course code");
        C44.setCellValue("Sum of Learning time (hrs)");
        D44.setCellValue("Sum of Number of trainees");
        E44.setCellValue("Sum of Number of enrolled trainees");
        F44.setCellValue("Sum of Number of graduates");
        G44.setCellValue("Sum of Expense");
        H44.setCellValue("Average of Training feedback");
        I44.setCellValue("Average of Training feedback - Content");
        J44.setCellValue("Average of Training feedback - Teacher");
        K44.setCellValue("Average of Training feedback - Organization");

        /*
         * RIGHT SECTIONS IMPLEMENT
         */
        N1.setCellValue("2. Exams & cert support");
        N2.setCellValue("2.1 By Start year & month");

        N5.setCellValue("Site");
        N6.setCellValue("Format type");
        N7.setCellValue("Delivery type");
        N8.setCellValue("Exam status");
        N9.setCellValue("Start Year");

        O5.setCellValue("(All)");
        O6.setCellValue("(All)");
        O7.setCellValue("(All)");
        O8.setCellValue("(All)");
        O9.setCellValue("(All)");

        N11.setCellValue("Row Labels");
        O11.setCellValue("Count of Exam code");
        P11.setCellValue("Sum of Expense");
        Q11.setCellValue("Average of Training feedback");

        N36.setCellValue("2.1 By End year & month");
        N38.setCellValue("Site");
        N39.setCellValue("Format type");
        N40.setCellValue("Delivery type");
        N41.setCellValue("Exam status");
        N42.setCellValue("End Year");

        O38.setCellValue("(All)");
        O39.setCellValue("(All)");
        O40.setCellValue("(All)");
        O41.setCellValue("(All)");
        O42.setCellValue("(All)");

        N44.setCellValue("Row Labels");
        O44.setCellValue("Count of Exam code");
        P44.setCellValue("Sum of Expense");
        Q44.setCellValue("Average of Training feedback");

        /*
         * STYLE (COLOR, BACKGROUND, FONT, ...)
         */
        CellStyle bgColor = bgStylePivots(sheet);
        CellStyle boldStyle = boldStylePivots(sheet);
        CellStyle boldBgStyle = boldBgStylePivots(sheet);

        // bold style
        A1.setCellStyle(boldStyle);
        A2.setCellStyle(boldStyle);
        N1.setCellStyle(boldStyle);
        N2.setCellStyle(boldStyle);
        A36.setCellStyle(boldStyle);
        N36.setCellStyle(boldStyle);

        // background color style
        A3.setCellStyle(bgColor);
        A4.setCellStyle(bgColor);
        A5.setCellStyle(bgColor);
        A6.setCellStyle(bgColor);
        A7.setCellStyle(bgColor);
        A8.setCellStyle(bgColor);
        A9.setCellStyle(bgColor);
        B3.setCellStyle(bgColor);
        B4.setCellStyle(bgColor);
        B5.setCellStyle(bgColor);
        B6.setCellStyle(bgColor);
        B7.setCellStyle(bgColor);
        B8.setCellStyle(bgColor);
        B9.setCellStyle(bgColor);

        N5.setCellStyle(bgColor);
        N6.setCellStyle(bgColor);
        N7.setCellStyle(bgColor);
        N8.setCellStyle(bgColor);
        N9.setCellStyle(bgColor);

        O5.setCellStyle(bgColor);
        O6.setCellStyle(bgColor);
        O7.setCellStyle(bgColor);
        O8.setCellStyle(bgColor);
        O9.setCellStyle(bgColor);

        A37.setCellStyle(bgColor);
        A38.setCellStyle(bgColor);
        A39.setCellStyle(bgColor);
        A40.setCellStyle(bgColor);
        A41.setCellStyle(bgColor);
        A42.setCellStyle(bgColor);

        B37.setCellStyle(bgColor);
        B38.setCellStyle(bgColor);
        B39.setCellStyle(bgColor);
        B40.setCellStyle(bgColor);
        B41.setCellStyle(bgColor);
        B42.setCellStyle(bgColor);

        N38.setCellStyle(bgColor);
        N39.setCellStyle(bgColor);
        N40.setCellStyle(bgColor);
        N41.setCellStyle(bgColor);
        N42.setCellStyle(bgColor);

        O38.setCellStyle(bgColor);
        O39.setCellStyle(bgColor);
        O40.setCellStyle(bgColor);
        O41.setCellStyle(bgColor);
        O42.setCellStyle(bgColor);

        // bold and background color style
        A11.setCellStyle(boldBgStyle);
        B11.setCellStyle(boldBgStyle);
        C11.setCellStyle(boldBgStyle);
        D11.setCellStyle(boldBgStyle);
        E11.setCellStyle(boldBgStyle);
        F11.setCellStyle(boldBgStyle);
        G11.setCellStyle(boldBgStyle);
        H11.setCellStyle(boldBgStyle);
        I11.setCellStyle(boldBgStyle);
        J11.setCellStyle(boldBgStyle);
        K11.setCellStyle(boldBgStyle);

        N11.setCellStyle(boldBgStyle);
        O11.setCellStyle(boldBgStyle);
        P11.setCellStyle(boldBgStyle);
        Q11.setCellStyle(boldBgStyle);

        A44.setCellStyle(boldBgStyle);
        B44.setCellStyle(boldBgStyle);
        C44.setCellStyle(boldBgStyle);
        D44.setCellStyle(boldBgStyle);
        E44.setCellStyle(boldBgStyle);
        F44.setCellStyle(boldBgStyle);
        G44.setCellStyle(boldBgStyle);
        H44.setCellStyle(boldBgStyle);
        I44.setCellStyle(boldBgStyle);
        J44.setCellStyle(boldBgStyle);
        K44.setCellStyle(boldBgStyle);

        N44.setCellStyle(boldBgStyle);
        O44.setCellStyle(boldBgStyle);
        P44.setCellStyle(boldBgStyle);
        Q44.setCellStyle(boldBgStyle);

        /*
         * DRAW BORDER
         */
        // left section
        borderPivots(new CellRangeAddress(7, 8, 0, 1), sheet);
        borderPivots(new CellRangeAddress(10, 10, 0, 10), sheet);
        borderPivots(new CellRangeAddress(41, 41, 0, 1), sheet);
        borderPivots(new CellRangeAddress(43, 43, 0, 10), sheet);

        // right section
        borderPivots(new CellRangeAddress(41, 41, 13, 14), sheet);
        borderPivots(new CellRangeAddress(8, 8, 13, 14), sheet);
        borderPivots(new CellRangeAddress(43, 43, 13, 16), sheet);
        borderPivots(new CellRangeAddress(10, 10, 13, 16), sheet);

    }
    private static CellStyle bgStylePivots(Sheet sheet){
        // create cellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

        // set color here
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // return result
        return cellStyle;
    }

    private static CellStyle boldStylePivots(Sheet sheet){
        // create font
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);

        // set font
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);

        // return bold font
        return cellStyle;
    }

    private static CellStyle boldBgStylePivots(Sheet sheet){
        // create font
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);

        // set color here
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        cellStyle.setWrapText(true);
        cellStyle.setVerticalAlignment(VerticalAlignment.BOTTOM);
        cellStyle.setAlignment(HorizontalAlignment.LEFT);

        return cellStyle;
    }
    private static void borderPivots(CellRangeAddress region, Sheet sheet){
        RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet);
        RegionUtil.setLeftBorderColor(IndexedColors.BLUE.index, region, sheet);
    }
    private static void borderInChanges(CellRangeAddress region, Sheet sheet){
//        RegionUtil.setBorderBottom(BorderStyle.DASHED, region, sheet);
//        System.out.println("heee");
//        RegionUtil.setLeftBorderColor(IndexedColors.BLUE.index, region, sheet);
        RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet);
        RegionUtil.setLeftBorderColor(IndexedColors.BLUE.index, region, sheet);
    }
}